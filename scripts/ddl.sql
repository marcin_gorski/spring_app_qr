-- drop database if exists contacts;
CREATE DATABASE contacts;
CREATE USER 'student'@'localhost'
  IDENTIFIED BY 'student';
GRANT ALL ON *.* TO 'student'@'localhost'
IDENTIFIED BY 'student';

USE contacts;

-- mysql -u root -p < ddl.sql
-- Enter password: root


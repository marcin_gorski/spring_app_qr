
-- users for security

USE contacts;

DROP TABLE IF EXISTS users;
CREATE TABLE users
(
  username VARCHAR(50) NOT NULL,
  password VARCHAR(50) NOT NULL,
  enabled boolean NOT NULL,
  PRIMARY KEY (username)   
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


 DROP TABLE IF EXISTS authorities;
CREATE TABLE authorities
(
  username VARCHAR(50) NOT NULL,
  authority VARCHAR(50) NOT NULL,
  CONSTRAINT fk_authorities_users FOREIGN KEY (username)
      REFERENCES users (username) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  UNIQUE(username, authority)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


INSERT INTO users VALUES ('qruser', 'password', true);

INSERT INTO authorities VALUES ('qruser', 'ROLE_USER');
INSERT INTO authorities VALUES ('qruser', 'ROLE_ADMIN');
INSERT INTO authorities VALUES ('qruser', 'ROLE_QR');

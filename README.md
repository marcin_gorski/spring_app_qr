# Spring framework - QR generator + Spring Security simple auth #

This is simple application created for j-labs Akademia presentation.
More info: http://www.j-labs.pl.

### Application info ###

* simple Spring MVC application
* contains one controller, that generates QR code via [zxing](https://github.com/zxing/zxing) library
* contains Spring Security authentication, that connects to mysql database (scripts included)

### Quickstart ###

* Download sources
* Execute SQL scripts from scripts/ (create database, add users)
* Start application (mvn clean tomcat:run)
* Go to http://localhost:8080/qrapp (qruser/password)

### Author ###

Marcin Górski [j-labs](http://j-labs.pl) [mgorski.net](http//mgorski.net)